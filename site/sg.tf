resource "aws_security_group" "alb_sg" {

  name        = "${substr(var.prefix, 0, 28)}-alb"
  description = "controls access to the alb"
  vpc_id      = var.vpc_id

  ingress {
    description = "Allow all http traffic"
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow all https traffic"
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${substr(var.prefix, 0, 24)}-alb"
  }
}

resource "aws_security_group" "ecs_sg" {

  name        = "${substr(var.prefix, 0, 20)}-ecs-sg"
  description = "ecs services sg - allow inbound traffic from alb only"
  vpc_id      = var.vpc_id

  ingress {
    description     = "http traffic from alb"
    protocol        = "tcp"
    from_port       = 80
    to_port         = 80
    security_groups = [aws_security_group.alb_sg.id]
  }

  ingress {
    description     = "ephemeral traffic from alb"
    from_port       = 0
    to_port         = 65535
    protocol        = "6"
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${substr(var.prefix, 0, 20)}-ecs-sg"
  }
}
